> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4930 Advanced Mobile Application Development

## Patrick Mahoney

### Assignment 1 Requirements:

*Includes:*

1. Configuring Git and BitBucket
2. Java Installation
3. Android Studio Installation and first apps
4. Chapter 1 & 2 questions

#### README.md file should include the following items:

* Screenshot of JDK installation
* Screenshot of Android Studio My First App
* Screenshot of Android Studio Contact App
* Git commands with short descriptions
*Bitbucket repo links 1) this assignment and 2) completed tutorial repos

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - create an empty git repository or reinitialize an existing one
2. git-status - Show the working tree status
3. git-add - Add file contents to the index
4. git-commit - Record changes to the repository
5. git-push - Update remote refs along with associated objects
6. git-pull - Fetch from and integrate with another repository or a local branch
7. git rm -Removes files from your index and your working directory so they will not be tracked

#### Assignment Screenshots:

*Screenshot of Contacts Page*:

![Contact Page Screenshot](img/contact_page1.png)

![Contact Page Screenshot](img/contact_page2.png)

![Contact Page Screenshot](img/contact_page3.png)

![Contact Page Screenshot](img/contact_page4.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android_hello.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
