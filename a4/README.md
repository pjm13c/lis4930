# LIS 4930 Advanced Mobile Application Development

## Patrick Mahoney

### Assignment 4 Requirements: 3 screenshots

*Includes:*

1. Creating an app that calculates a user's total mortgage payment with persistent data.

#### README.md file should include the following items:

* Screenshot of splash screen
* Screenshot of invalid entry
* Screenshot of valid entry.

#### Assignment Screenshots:

*Screenshot of splash screen*:

![Unpopulated user interface](img/splash_screen.png)

*Screenshot of valid entry*:

![Converted currency](img/valid_entry.png)

*Screenshot of invalid entry.*:

![Toast Notification](img/invalid_entry.png)

