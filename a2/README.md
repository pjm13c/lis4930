# LIS 4930 Advanced Mobile Application Development

## Patrick Mahoney

### Assignment 2 Requirements: 3 screenshots

*Includes:*

1. Creating an app that calculates the tip for a group of people.
2. Chapter 3 Questions

#### README.md file should include the following items:

* Screenshot of unpopulated application
* Screenshot of populated application
* Screenshot of launcher icon in menu.

#### Assignment Screenshots:

*Screenshot of unpopulated application*:

![AMPPS Installation Screenshot](img/unpopulated.png)

*Screenshot of populated application*:

![JDK Installation Screenshot](img/populated.png)

*Screenshot of launcher icon.*:

![Android Studio Installation Screenshot](img/extra_credit_launcher_icon.png)

