# LIS 4930 Advanced Mobile Application Development

## Patrick Mahoney

### Project 1 Requirements: 5 screenshots

*Includes:*

1. Creating an app that plays and pauses an mp3 file after a button click.

#### README.md file should include the following items:

* Screenshot of launcher icon
* Screenshot of splash screen.
* Screenshot of App running.
* Screenshot of song playing.
* Screenshot of song paused.

#### Assignment Screenshots:

*Screenshot of launcher icon*:

![Launcher Icon](img/launcher_icon.png)

*Screenshot of splash screen*:

![Spalsh Screen](img/splash_image.png)

*Screenshot of App running.*:

![App Running](img/first_screen.png)

*Screenshot of song playing*:

![Song Playing](img/song_playing.png)

*Screenshot of song paused*

![Song Paused](img/song_paused.png)
