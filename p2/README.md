# LIS 4930 Advanced Mobile Application Development

## Patrick Mahoney

### Project 2 Requirements: 1 screenshot

*Includes:*

1. Creating an app uses hardcoded values to create a tasklist

#### README.md file should include the following items:

* Screenshot of running application

#### Assignment Screenshots:

*Screenshot of running application*:

![App runnning](img/working_app.png)

