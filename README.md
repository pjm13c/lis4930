# LIS 4930 Advanced Mobile Application Development

## Patrick Mahoney

*Course Work Links*

[A1 README.MD](a1/README.md "MY A1 README.md FILE") 
    
    * Working in JDK environment
    * Beginning Android Studio
    * Creating README.md files

[A2 README.MD](a2/README.md "MY A2 README.md FILE") 
   
    * Using Spinners in Android Studio
    * Using Text Entry in Android Studio
    * Adding a launcher image in Android Studio

[A3 README.MD](a3/README.md "MY A3 README.MD FILE") 
   
    * Using radio buttons to allow users to choose an option.
    * Using a launcher image in Android Studio.
    * Using a toast notification in Android Studio.

[P1 README.MD](p1/README.md "MY P1 README.MD FILE") 
   
    * Showing a splash screen to users while content loads.
    * Implementing background images on a textView widget.
    * Playing music in an Android application.

[A4 README.MD](a4/README.md "MY A4 README.MD FILE")

	*Using persistent data
	*Loading an image based on user input

[P2 README.MD](p2/README.md "MY PROJECT 2 README.MD FILE")

	*Using a database with an Android Application
	*Using sql with java and Android Applications