# LIS 4930 Advanced Mobile Application Development

## Patrick Mahoney

### Assignment 3 Requirements: 3 screenshots

*Includes:*

1. Creating an app that converts currency from USD to Euro, Peso, and CAD.

#### README.md file should include the following items:

* Screenshot of unpopulated application
* Screenshot of populated application
* Screenshot of toast notification.

#### Assignment Screenshots:

*Screenshot of unpopulated application*:

![Unpopulated user interface](img/unpopulated.png)

*Screenshot of populated application*:

![Converted currency](img/populated.png)

*Screenshot of toast notification.*:

![Toast Notification](img/toast.png)

